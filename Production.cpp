//
// Created by Adam on 04/12/2019.
//

#include "Company.h"
#include "Production.h"
#include <string>
extern float* cashPtr;
extern Storage* storagePtr;
extern Company* companyPtr;

Production::Production() {
    m_hoursWorked=0;
}

class Machine * Production::getMachine(const string& machineName) {
    for(auto i : m_machines){
        if(i->getName()==machineName){
            return i;
        }
    }
    return nullptr;
}

void Production::buyMachine(const string& name, int amount) {
    if(amount<=0){
        cout << "Wrong input! try again." << endl;
    } else {
        auto varMachine = getMachine(name);
        if (varMachine->getPrice() * amount > *cashPtr) {
            cout << "You don't have enough money to buy that machine!" << endl;
        } else {
            companyPtr->setCash((-1) * varMachine->getPrice());
            varMachine->setAmount(amount);
        }
    }
}

void Production::sellMachine(string name, int amount) {
    if(amount<=0){
        cout << "Wrong input! try again." << endl;
    } else {
        auto varMachine = getMachine(name);
        companyPtr->setCash(varMachine->getPrice() / 2);
        varMachine->setAmount((-1) * amount);
    }
}
