//
// Created by Adam on 04/12/2019.
//

#include "Item.h"
#include "Storage.h"
#include "Company.h"
#include <iostream>
extern Storage* storagePtr;
extern Company* companyPtr;

Item::Item(string name, float price, float amount, float spaceRequired) {
    m_name = name;
    m_price = price;
    m_amount = amount;
    m_spaceRequired = spaceRequired;
}

string Item::getName() {
    return m_name;
}

float Item::getAmount() {
    return m_amount;
}

float Item::getPrice() {
    return m_price;
}

float Item::getSpaceRequired() {
    return m_spaceRequired;
}

void Item::setAmount(float difference) {
    storagePtr->changeUsedCapacity(difference*m_spaceRequired);
    m_amount += difference;
}

void Item::printInfo() {
    cout << "- " << m_name.c_str() << ", price: " << m_price << ", amount: " << m_amount << ", required space: " << m_spaceRequired << endl;
}