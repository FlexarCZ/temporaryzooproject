//
// Created by Adam on 04/12/2019.
//

#ifndef ZOOPROJECT_STORAGE_H
#define ZOOPROJECT_STORAGE_H

#include "Material.h"
#include "Product.h"


class Storage {
    float m_capacity;
    float m_capacityMultiplier;
    float m_usedCapacity;
public:
    vector<Material*> m_materials;
    vector<Product*> m_products;
    Storage();
    void sellItem(const string& itemName, float amount);
    void buyItem(string itemName, float amount);
    void sellMaterial(const string& name, float amount);
    Material* getMaterial(const string& materialName);
    Item* getItem(const string& itemName);
    Product* getProduct(const string& productName);
    float getRemainingCapacity();
    float getTotalCapacity();
    void changeUsedCapacity(float difference);
    void increaseTotalCapacity(int amount);
    void printInfo();
};


#endif //ZOOPROJECT_STORAGE_H
