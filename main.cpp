#include <iostream>
#include <string>
#include "Company.h"
#include "InputScan.h"

extern Storage* storagePtr;
extern Production* productionPtr;
Company* companyPtr = new Company(15000);



int main() {
    auto scanInput = new InputScan();
//    auto myCompany = new Company(15000);
//    myCompany->s_storage->increaseTotalCapacity(1);

    auto woodMaterial = new Material("wood", 10, 1);
    auto ironMaterial = new Material("iron", 25, 2);
    vector<ItemsNeeded *> itemsNeededVar = {new ItemsNeeded(woodMaterial, 5)};
    auto woodChair = new Product("woodChair", 200, itemsNeededVar, 5, 4);
    itemsNeededVar.push_back(new ItemsNeeded(ironMaterial, 2));
    auto woodIronChair = new Product("woodIronChair", 300, itemsNeededVar, 8, 5);

    storagePtr->m_materials.push_back(woodMaterial);
    storagePtr->m_materials.push_back(ironMaterial);
    storagePtr->m_products.push_back(woodChair);
    storagePtr->m_products.push_back(woodIronChair);

    auto machine1 = new Machine("machine1", 300, 5000, 50);
    productionPtr->m_machines.push_back(machine1);
    productionPtr->buyMachine("machine1", 1);

    string playerInput;
    while(playerInput != "exit"){
        getline(cin,playerInput);
        vector<string> splitInput = scanInput->splitInput(playerInput);
        scanInput->processInput(splitInput);
    }
    return EXIT_SUCCESS;
}

