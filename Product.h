//
// Created by Adam on 04/12/2019.
//

#ifndef ZOOPROJECT_PRODUCT_H
#define ZOOPROJECT_PRODUCT_H


#include "Item.h"
#include "Material.h"
#include "ItemsNeeded.h"
#include <vector>

class Product : public Item {
    vector<ItemsNeeded *> m_itemsNeeded;
    float m_timeNeeded;
public:
    Product(string name, float price, vector<ItemsNeeded *> itemsNeeded, float timeNeeded, float spaceRequired);
    vector<ItemsNeeded *> getItemsNeeded();
    float getTimeNeeded();
};


#endif //ZOOPROJECT_PRODUCT_H
