//
// Created by Adam on 11/12/2019.
//

#include "ItemsNeeded.h"

ItemsNeeded::ItemsNeeded(Item * itemNeeded, float amount) {
    m_itemNeeded = itemNeeded;
    m_amount = amount;
}

class Item * ItemsNeeded::getItemNeeded() {
    return m_itemNeeded;
}

float ItemsNeeded::getAmountNeeded() {
    return m_amount;
}