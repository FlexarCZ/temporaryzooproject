//
// Created by Adam on 04/12/2019.
//

#include "Machine.h"

Machine::Machine(string name, float productionRate, float price, float costPerHour) {
    m_name = name;
    m_productionRate = productionRate;
    m_price = price;
    m_costPerRound = costPerHour;
    m_amount = 0;
}

float Machine::getProductionRate() {
    return m_productionRate;
}

float Machine::getPrice() {
    return m_price;
}

string Machine::getName() {
    return m_name;
}

float Machine::getCostPerRound() {
    return m_costPerRound;
}

int Machine::getAmount() {
    return m_amount;
}

void Machine::setAmount(int difference) {
    m_amount += difference;
}

void Machine::printInfo() {
    cout << "- " << m_name.c_str() << ", price: " << m_price << ", amount: " << m_amount;
    cout << ", total production Rate: " << m_productionRate*m_amount << ", total cost per round: " << m_costPerRound*m_amount << endl;
}